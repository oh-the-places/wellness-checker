Wellness Checker
---

### Building

1. Run `$ composer install`
2. Normally, you'd then run `$ npm install` to install frontend dependencies. But something is broken with the pngquant lib in npm, so you have to do the following:

    1. `$ sudo apt-get install libpng-dev`
    2. `$ npm install pngquant-bin // ensure node_modules doesn't exist` 
    3. `$ npm install`
    
    More info: https://github.com/imagemin/pngquant-bin/issues/78
    
3. Run `$ npm run dev` (for development) to build the Vue.js stuff
4. Run migrations and seeds:
vendor/laravel/framework/src/Illuminate/Foundation/helpers.php
    1. `$ php artisan migrate`
    2. `$ php artisan db:seed`

    Note that users are not seeded to ensure new suers can go through the registration process
    
### Testing

1. You should create a new testing db before running tests. The configuration for the db name is in `phpunit.xml`, where the default is `wellness_checker-testing`. Alternatively, 