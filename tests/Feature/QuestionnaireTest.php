<?php

namespace Tests\Feature;

use App\Models\Auth\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class QuestionnaireTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Ensures that the endpoints require auth
     *
     * @return void
     */
    public function testAuth(){
        $response = $this->json('GET', '/api/question');
        $response
            ->assertStatus(401);


    }

    public function testIndex(){
        $this->createQuestionsAndAnswers();

        $user = factory(User::class)->create();
        $response = $this->actingAs($user, 'api')
            ->get('/api/question');

        $response
            ->assertJsonStructure([
                'data' => [
                    [
                        'id',
                        'value',
                        'question_choices' => [
                            [
                                'id',
                                'value',
                            ]
                        ]
                    ],
                ],
                'message'
            ])
            ->assertStatus(200);
    }

    public function testStoreAnswers(){
        $this->createQuestionsAndAnswers();

        $user = factory(User::class)->create();

        $response = $this->actingAs($user, 'api')
            ->post('/api/question/storeAnswers', ['selected_answers' => '{"1":1,"2":4,"3":7}']);

        $response
            ->assertJsonStructure([
                'data' => [
                    [
                        'id',
                        'value',
                        'question_choices' => [
                            [
                                'id',
                                'value',
                            ]
                        ]
                    ],
                ],
                'message'
            ])
            ->assertStatus(200);
    }

    private function createQuestionsAndAnswers(){
        $questionValue = 'How many meals have you had today?';
        factory(\App\Models\Questionnaire\Question::class)->create([
            'value' => $questionValue
        ])->each(function (\App\Models\Questionnaire\Question $question) use ($questionValue) {
            if($question->value === $questionValue){
                $question->questionChoices()->save(factory(\App\Models\Questionnaire\QuestionChoice::class)->make(['value' => '1']));
                $question->questionChoices()->save(factory(\App\Models\Questionnaire\QuestionChoice::class)->make(['value' => '2']));
                $question->questionChoices()->save(factory(\App\Models\Questionnaire\QuestionChoice::class)->make(['value' => '3']));
            }
        });

        $questionValue = 'Question Two?';
        factory(\App\Models\Questionnaire\Question::class)->create([
            'value' => $questionValue
        ])->each(function (\App\Models\Questionnaire\Question $question) use ($questionValue) {
            if($question->value === $questionValue){
                $question->questionChoices()->save(factory(\App\Models\Questionnaire\QuestionChoice::class)->make(['value' => '1']));
                $question->questionChoices()->save(factory(\App\Models\Questionnaire\QuestionChoice::class)->make(['value' => '2']));
                $question->questionChoices()->save(factory(\App\Models\Questionnaire\QuestionChoice::class)->make(['value' => '3']));
            }
        });

        $questionValue = 'Question Three?';
        factory(\App\Models\Questionnaire\Question::class)->create([
            'value' => $questionValue
        ])->each(function (\App\Models\Questionnaire\Question $question) use ($questionValue) {
            if($question->value === $questionValue){
                $question->questionChoices()->save(factory(\App\Models\Questionnaire\QuestionChoice::class)->make(['value' => '1']));
                $question->questionChoices()->save(factory(\App\Models\Questionnaire\QuestionChoice::class)->make(['value' => '2']));
                $question->questionChoices()->save(factory(\App\Models\Questionnaire\QuestionChoice::class)->make(['value' => '3']));
            }
        });
    }
}
