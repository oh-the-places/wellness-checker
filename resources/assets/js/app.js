
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import Vue from 'vue';
import bootstrap from './bootstrap';

import PassportOauthControls from './components/passport/OauthControls.vue';
import QuestionList from './components/questionnaire/QuestionList.vue';

Vue.component('passport-oauth-controls', PassportOauthControls);
Vue.component('question-list', QuestionList);

// Creating two Vue objects, where $eventHub is a simple, global event management system. Without this, we could not
// $emit up to grandparents and beyond without building an ugly emit chain
Vue.prototype.$eventHub = new Vue();
new Vue({
    el: '#app',
});