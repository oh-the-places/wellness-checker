@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <question-list></question-list>
        </div>
    </div>
</div>
@endsection
