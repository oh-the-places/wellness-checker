@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h2>OAuth Dashboard</h2>
            <passport-oauth-controls></passport-oauth-controls>
        </div>
    </div>
</div>
@endsection
