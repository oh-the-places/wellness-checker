<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToQuestionAnswerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('question_answer', function(Blueprint $table)
		{
			$table->foreign('user_id', 'FK_28')->references('id')->on('user')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('question_choice_id', 'FK_32')->references('id')->on('question_choice')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('question_id', 'FK_36')->references('id')->on('question')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('question_answer', function(Blueprint $table)
		{
			$table->dropForeign('FK_28');
			$table->dropForeign('FK_32');
			$table->dropForeign('FK_36');
		});
	}

}
