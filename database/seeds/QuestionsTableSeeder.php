<?php

use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $questionValue = 'How many meals have you had today?';
        factory(App\Models\Questionnaire\Question::class)->create([
            'value' => $questionValue
        ])->each(function (App\Models\Questionnaire\Question $question) use ($questionValue) {
            if($question->value === $questionValue){
                $question->questionChoices()->save(factory(App\Models\Questionnaire\QuestionChoice::class)->make(['value' => '1']));
                $question->questionChoices()->save(factory(App\Models\Questionnaire\QuestionChoice::class)->make(['value' => '2']));
                $question->questionChoices()->save(factory(App\Models\Questionnaire\QuestionChoice::class)->make(['value' => '3']));
            }
        });

        $questionValue = 'How many hours did you sleep last night?';
        factory(App\Models\Questionnaire\Question::class)->create([
            'value' => $questionValue
        ])->each(function (App\Models\Questionnaire\Question $question) use ($questionValue) {
            if($question->value === $questionValue){
                $question->questionChoices()->save(factory(App\Models\Questionnaire\QuestionChoice::class)->make(['value' => '0 - 3']));
                $question->questionChoices()->save(factory(App\Models\Questionnaire\QuestionChoice::class)->make(['value' => '3 - 6']));
                $question->questionChoices()->save(factory(App\Models\Questionnaire\QuestionChoice::class)->make(['value' => '6 - 8']));
                $question->questionChoices()->save(factory(App\Models\Questionnaire\QuestionChoice::class)->make(['value' => '8+']));
            }
        });

        $questionValue = 'How many minutes of exercise do you have planned for today?';
        factory(App\Models\Questionnaire\Question::class)->create([
            'value' => $questionValue
        ])->each(function (App\Models\Questionnaire\Question $question) use ($questionValue) {
            if($question->value === $questionValue){
                $question->questionChoices()->save(factory(App\Models\Questionnaire\QuestionChoice::class)->make(['value' => '0 - 10']));
                $question->questionChoices()->save(factory(App\Models\Questionnaire\QuestionChoice::class)->make(['value' => '10 - 30']));
                $question->questionChoices()->save(factory(App\Models\Questionnaire\QuestionChoice::class)->make(['value' => '30 - 60']));
                $question->questionChoices()->save(factory(App\Models\Questionnaire\QuestionChoice::class)->make(['value' => '60+']));
            }
        });
    }
}
