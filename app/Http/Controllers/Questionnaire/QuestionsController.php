<?php

namespace App\Http\Controllers\Questionnaire;

use App\Http\Controllers\Controller;
use App\Models\Auth\User;
use App\Models\Questionnaire\Question;
use App\Models\Questionnaire\QuestionAnswer;
use Doctrine\DBAL\DBALException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

/**
 * Class QuestionsController
 * @package App\Http\Controllers\Auth
 */
class QuestionsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Returns all questions with answers
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request):JsonResponse{
        $allQuestions = Question::with('questionChoices')->get();

        return $this->formattedData($allQuestions);
    }

    /**
     * Get all answered questions for a specific user
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getAnswersForUser(Request $request):JsonResponse{
        $this->validate($request, [
            'user_id' => 'required|between:1:10',
        ]);

        $userId = $request->input('user_id');
        // In the future, potentially add functionality to ensure a user has access rights to another user
        $user = User::with([
            'questionAnswers.questionChoice',
            'questionAnswers.question.'
            ])
            ->find($userId);

        return $this->formattedData($user->questionAnswers);
    }

    /**
     * Answers a question by its ID. The choice ID is passed as param
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function storeAnswers(Request $request):JsonResponse{
        $this->validate($request, [
            'selected_answers' => 'required|string',
        ]);

        $user = Auth::user();
        if($user === null){
            return $this->formattedData(null, 'There was no session\'d user. Try logging out and back in.', 400);
        }
        $userId = $user->getKey();

        $selectedAnswers = json_decode($request->input('selected_answers'));
        $choicesMade = [];
        foreach ($selectedAnswers as $questionId => $choiceId){
            $questionAnswer = new QuestionAnswer();
            $questionAnswer->question_choice_id = $choiceId;
            $questionAnswer->question_id = $questionId;
            $questionAnswer->user_id = $userId;

            try{
                $questionAnswer->save();
                $choicesMade[] = $choiceId;
            }
            catch(QueryException $dbException){
                Log::error($dbException);
                return $this->formattedData(null, 'Database exception, possibly due to FK constraints. Check logs.', 400);
            }
        }

        $questionsWithAnswers = Question::with([
            'questionChoices' => function($query) use ($choicesMade){
                $query->whereIn('id', $choicesMade);
            },
        ])->get();

        return $this->formattedData($questionsWithAnswers, 'Answers successfully saved.');
    }
}
