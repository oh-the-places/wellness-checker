<?php

namespace App\Models\Auth;

use App\Models\Questionnaire\QuestionAnswer;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

/**
 *
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $rememberToken
 *
 * Relations
 * ---
 * @property Collection questionAnswers
 */
class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    protected $table = 'user';

    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $visible = [
        'name',
        'email',

        'questionAnswers'
    ];

    public function questionAnswers(){
        return $this->hasMany(QuestionAnswer::class, 'user_id', 'id');
    }
}
