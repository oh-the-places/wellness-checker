<?php

namespace App\Models\Questionnaire;

use Illuminate\Database\Eloquent\Model;

/**
 *
 * @property string value
 */
class Question extends Model
{
    protected $table = 'question';

    protected $visible = [
        'id',
        'value',
        'questionChoices',
        'answeredChoices'
    ];

    public function questionChoices(){
        return $this->hasMany(QuestionChoice::class, 'question_id', 'id');
    }

    /**
     * This is meant to be used to figure out
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function answeredChoices(){
        return $this->hasManyThrough(
            QuestionAnswer::class,
            QuestionChoice::class,
            'question_id',
            'question_choice_id'
        );
    }
}
