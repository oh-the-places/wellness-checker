<?php

namespace App\Models\Questionnaire;

use App\Models\Auth\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 *
 * @property int $question_choice_id
 * @property string $question_id
 * @property string $user_id
 */
class QuestionAnswer extends Model
{
    protected $table = 'question_answer';

    protected $visible = [
        'question_id',
        'question_choice_id',

        'question',
        'questionChoice',
    ];

    public function question(){
        return $this->belongsTo(Question::class, 'question_choice_id', 'id');
    }

    public function questionChoice(){
        return $this->belongsTo(QuestionChoice::class, 'question_choice_id', 'id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'question_choice_id', 'id');
    }

    public function scopeOfUserId(Builder $builder, $userId){
        return $builder
            ->where('user_id', '=', $userId);
    }

    public function scopeOfLastAnswered(Builder $builder){
        $questionAnswerTable = $this->table;
        return $builder
                ->where("$questionAnswerTable.id", function ($sub) use ($questionAnswerTable){
                    $sub->from("$questionAnswerTable as sub")
                        ->selectRaw("max($questionAnswerTable.id)")
                        ->whereRaw("sub.question_id = $questionAnswerTable.question_id");
                });
    }
}
