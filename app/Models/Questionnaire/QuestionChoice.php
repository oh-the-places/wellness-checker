<?php

namespace App\Models\Questionnaire;

use Illuminate\Database\Eloquent\Model;

/**
 *
 * @property int $question_id
 * @property int $value
 */
class QuestionChoice extends Model
{
    protected $table = 'question_choice';

    protected $visible = [
        'id',
        'value',
        'question',
    ];

    public function question(){
        return $this->belongsTo(Question::class, 'question_id', 'id');
    }
}
